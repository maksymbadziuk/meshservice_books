DROP TABLE IF EXISTS books;

CREATE TABLE books (
                       id INT AUTO_INCREMENT PRIMARY KEY,
                       name VARCHAR(250) NOT NULL
);

INSERT INTO books (name) VALUES
( 'War and Piece'),
( 'Anna Karenina'),
( 'Hobbit'),
( 'Client'),
( 'Star wars'),
( 'The cat and the dog');