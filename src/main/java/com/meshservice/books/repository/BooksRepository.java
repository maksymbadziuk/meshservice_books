package com.meshservice.books.repository;

import com.meshservice.books.entity.Book;
import org.springframework.data.repository.CrudRepository;

public interface BooksRepository extends CrudRepository<Book, Long> {


}
