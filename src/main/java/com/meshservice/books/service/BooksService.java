package com.meshservice.books.service;

import com.meshservice.books.model.BookDto;
import com.meshservice.books.repository.BooksRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BooksService {

    @Autowired
    private BooksRepository booksRepository;

    public List<BookDto> getAllBooks() {
        return BookDto.transformEntities(this.booksRepository.findAll());
//        return List.of(new Book("War and Piece"), new Book("The War is mine"));
    }
}
