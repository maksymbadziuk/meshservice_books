package com.meshservice.books.model;

import com.meshservice.books.entity.Book;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Data
@AllArgsConstructor
@ToString
public class BookDto {
    private Long id;
    private String name;

    private Double rating;
    private String presentation;

    public BookDto(final Long id, final String name) {
        this.id = id;
        this.name = name;
    }

    public static List<BookDto> transformEntities(final Iterable<Book> entities) {
        return StreamSupport.stream(entities.spliterator(), false)
                .map(entity -> new BookDto(entity.getId(), entity.getName()))
                .collect(Collectors.toList());
    }
}
