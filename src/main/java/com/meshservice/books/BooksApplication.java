package com.meshservice.books;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

import javax.annotation.PreDestroy;

@SpringBootApplication
@EnableFeignClients
@EnableDiscoveryClient
public class BooksApplication {

    public static void main(String[] args) {
        SpringApplication.run(BooksApplication.class, args);
    }

    // To gracefully finish work after container is terminated by ECS
    @PreDestroy
    public void tearDown() {
        System.out.println("Shutting Down...............");
    }
}
