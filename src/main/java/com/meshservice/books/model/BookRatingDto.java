package com.meshservice.books.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@AllArgsConstructor
@ToString
public class BookRatingDto {

    private Long id;
    private Double rating;
    private String presentation;

    public static BookRatingDto defaultObj() {
        return new BookRatingDto(0l, 0.0, ":-)");
    }
}
