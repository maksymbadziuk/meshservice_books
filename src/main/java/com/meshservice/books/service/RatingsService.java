package com.meshservice.books.service;

import com.meshservice.books.clients.RatingsClient;
import com.meshservice.books.model.BookRatingDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class RatingsService {

    Logger log = LoggerFactory.getLogger(RatingsService.class);

    @Autowired
    private RatingsClient ratingsClient;

    public List<BookRatingDto> getBooksRatingsByIds(final List<Long> ids) {
        final List<BookRatingDto> booksRatingsByIds = ratingsClient.getBooksRatingsByIds(StringUtils.collectionToCommaDelimitedString(ids));

        log.info(booksRatingsByIds.toString());

        return booksRatingsByIds;
    }
}
