package com.meshservice.books.clients;

import com.meshservice.books.model.BookRatingDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient(name="ratings", url="service.internal:8080")
//@FeignClient(name="ratings", url="ratings")
//@FeignClient(value = "ratings-ws", url = "http://localhost:8081")
public interface RatingsClient {

    @RequestMapping(method= RequestMethod.GET, value = "/ratings/{ids}")
    List<BookRatingDto> getBooksRatingsByIds(final @PathVariable("ids") String ids);

}
