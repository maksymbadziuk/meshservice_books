package com.meshservice.books.controller;

import com.meshservice.books.clients.RatingsClient;
import com.meshservice.books.model.BookDto;
import com.meshservice.books.model.BookRatingDto;
import com.meshservice.books.service.BooksService;
import com.meshservice.books.service.RatingsService;
import org.apache.catalina.connector.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
public class BooksController {

    @Autowired
    private Environment environment;

    @Autowired
    private BooksService booksService;

    @Autowired
    private RatingsService ratingsService;

    @CrossOrigin
    @GetMapping(value="/")
    public String getRoot() {
        return "IP Address is: " +  environment.getProperty("local.server.address") ;
    }


    @CrossOrigin
    @GetMapping(value="/books")
    public List<BookDto> getBooks() {

        final List<BookDto> allBooks = this.booksService.getAllBooks();

        final List<Long> ids = allBooks.stream().map(book -> book.getId()).collect(Collectors.toList());

        final List<BookRatingDto> booksRatingsByIds = this.ratingsService.getBooksRatingsByIds(ids);

        final Map<Long, BookRatingDto> ratingsMap = booksRatingsByIds.stream().collect(Collectors.toMap(BookRatingDto::getId, bookRatingDto -> bookRatingDto));


        allBooks.stream()
                .forEach(bookDto -> {
                    final BookRatingDto rating = ratingsMap.getOrDefault(bookDto.getId(), BookRatingDto.defaultObj());
                    bookDto.setRating(rating.getRating());
                    bookDto.setPresentation(rating.getPresentation());
                });




        return allBooks;
    }
}
